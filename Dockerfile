FROM node:18

WORKDIR /app/discord

RUN npm install -g npm@latest \
	&& npm install -g npm-check-updates pnpm

# RUN pnpm install && pnpm build

# CMD [ "node", "dist/Bot.js" ]
ENTRYPOINT ["tail", "-f", "/dev/null"]