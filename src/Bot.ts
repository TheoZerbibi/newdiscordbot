import { Bot } from './client/Client'
import { Core } from './common/Core'

const core = new Core();
core.start().then(() => {
	const bot = new Bot(core);
	bot.start().then(() => {
		core.logger.success('Bot started');
	}).catch(err => {
		core.logger.error(err);
	});
});
