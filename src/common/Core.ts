import consola, { Consola } from 'consola';
import { Mongoose, Model, model } from 'mongoose';
import { config } from "dotenv";

export class Core {

	public static core: Core;

	public logger: Consola = consola;
	
	private mongoose: Mongoose = new Mongoose();
	private redisClient: any;

	constructor() {
		config();
	}

	public async start(): Promise<void> {
		await this.initializeMongo();
		await this.initializeRedis();
		this.logger.success('Core is ready!');
	}

	private async initializeRedis(): Promise<any> {
		const asyncRedis = require('async-redis');
		this.logger.info("┌ Initializing redis...");
		this.logger.info(`│ 🞄 Connecting to ${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`);
		this.redisClient = await asyncRedis.createClient({ url: `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`, password: process.env.REDIS_PASSWORD, timeout: 1000 });
		return new Promise(resolve => {
			this.redisClient.on('ready', () => {
				this.logger.success("└ Redis connection initialized !")
				resolve('success');
			});
			this.redisClient.on('error', (err: any) => {
				this.logger.error(`└ - Error while connecting to redis: ${err}`);
				resolve('error');
				process.exit(1);
			});
		});
	}

	private async initializeMongo(): Promise<void> {
		const pass = process.env.MONGO_PASSWORD?.replace(/./g, '*').slice(0, 6);;
		this.logger.info("┌ Initializing mongo...");
		this.logger.info(`│ 🞄 Connecting to ${process.env.MONGO_USER}:${pass}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`);
		const promise = this.mongoose.connect(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}/${process.env.MONGO_DB}`);
		await promise.catch(error => {
			this.logger.error(`└ - Error while connecting to mongo: ${error}`);
			process.exit(1);
		}).then(() => {
			this.logger.success("└ MongoDB connection initialized !")
		});
	}
}