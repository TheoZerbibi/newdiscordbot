import { Bot } from '../client/Client';

import { CommandInteraction } from 'discord.js';


export interface Command {

    name: string;

    description: string;

    permissions?: bigint[];

    aliases?: string[];

    run(client: Bot, interaction: CommandInteraction): Promise<void>;

    build(): any;

}
