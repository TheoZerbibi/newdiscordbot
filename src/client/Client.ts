// import {Client, Collection, Guild, GuildMember, IntentsBitField, Partials} from 'discord.js';
import { Client, IntentsBitField, Partials, Collection } from 'discord.js';
const { ActivityType } = require('discord.js');

import glob from 'glob';
import { promisify } from 'util';

import consola, { Consola } from 'consola';
import { Core } from '../common/Core';

import { Command } from '../interfaces/Command';

const globPromise = promisify(glob);

export class Bot extends Client {
	
	public static bot: Bot;

	public logger: Consola = consola;

	public core: Core;

	public commands: Collection<string, Command> = new Collection();

	public constructor(core: Core) {
		super({
			intents: [
				IntentsBitField.Flags.Guilds,
				IntentsBitField.Flags.GuildMessages,
				IntentsBitField.Flags.GuildMessageReactions,
				IntentsBitField.Flags.GuildMembers
			],
			partials: [Partials.Message, Partials.Channel, Partials.Reaction],
			presence: { activities: [ { name: "Test Bot", type: ActivityType.Playing , url: 'https://tzeribi.fr' } ] }
		});

		Bot.bot = this;
		this.core = core;
	}

	public async start(): Promise<void> {
		await this.loadCommands();

		await this.login(process.env.TOKEN);
	}

	private async loadCommands(): Promise<void> {
		const commandFiles: string[] = await globPromise(`${__dirname}/../commands/impls/**/*{.ts,.js}`);
		console.log(commandFiles);
		commandFiles.map(async (value: string) => {
			const file: any = await import(value);
			const command: Command = new file.default();
			this.commands.set(command.name, command);
		});
	}


	public static get(): Bot {
		return Bot.bot;
	}


	public getCore(): Core {
		return this.core;
	}

}