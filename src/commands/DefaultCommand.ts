import { Command } from '../interfaces/Command';
import { SlashCommandBuilder } from '@discordjs/builders';
import { Bot } from '../client/Client';
import { CommandInteraction } from 'discord.js';

export abstract class DefaultCommand implements Command {

	public name: string;
	public description: string;
	public permissions?: bigint[];
	public aliases?: string[];

	constructor(name: string, description: string) {
		this.name = name;
		this.description = description;
	}

	abstract run(client: Bot, interaction: CommandInteraction): Promise<void>;

	public build(): any {
		return new SlashCommandBuilder()
			.setName(this.name)
			.setDescription(this.description);
	}
}
