import { DefaultCommand } from '../DefaultCommand';
import { Bot } from '../../client/Client';
import { CommandInteraction } from 'discord.js';

export default class TestCommand extends DefaultCommand {

	constructor() {
		super('test', 'Test command');
	}

	public async run(client: Bot, interaction: CommandInteraction): Promise<void> {
		interaction.reply('Ok');
	}

}
